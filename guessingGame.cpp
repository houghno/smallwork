/*
The user picks a number between 1 and 100. The program guesses 
the user's number within 7 turns.
*/
#include "stdafx.h"
#include <iostream>
using namespace std;

int guessing(int userIn, int guess, int &high, int &low){
	if(userIn == 1){ //Number is higher
		low = guess;
		guess = (high / 2) + (guess * 0.5) + 1;
	}
	else{ //Number is lower
		high = guess;
		guess = ((guess - low) * 0.5) + low;
	}
	return guess;
}

int main(){
	const int MAX = 100;
	cout << "Think of a number between 1 and " << MAX << "." << endl;

	int guess = MAX / 2,
		turns = 1,
		high = MAX,
		low = 1,
		numberCheck = 0;

	while(!numberCheck){
		cout << "Is your number " << guess << "?" << endl;
		cout << "(1)Yes, (2)No: ";
		int userIn;
		cin >> userIn;

		if(userIn == 1){
			numberCheck = 1; //Found user's number
		}
		else{
			turns++;
			cout << "Is your number (1)higher or (2)lower?: ";
			cin >> userIn;
			guess = guessing(userIn, guess, high, low);
		}
	}
	cout << "Your number is " << guess << endl;
	cout << "Guessed your number in " << turns << " turns." << endl;
}